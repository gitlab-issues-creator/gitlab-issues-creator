# Usage

Let's see what functions we have in instance

# Primary functions and variables

## templatesWithIds (getter)

Returns same templates you gave in package options in array format with additional fields: `id` as index and `type` as
template key

## createIssue

Creates issue with all the parameters

### Options

* `title` (required) - string, issue title (without `prependTitle`, which will be inserted automatically)
* `description` (required) - string, issue text. This field accepts markdown format and is used along with `files`
  parameter. You can reference files by their index with format `[fileINDEX]`, for example, `[file0]`. If file with such
  an index does not exist in files, it will be skipped. If you does not reference all files in description, they will be
  placed in the bottom of description with original order.
* `personalAccessToken` - token to **create** issue with. Ignored if `oAuthAccessToken` exists.
* `oAuthAccessToken` - oAuth token to **create** issue with.

Notice: if both `personalAccessToken` and `oAuthAccessToken` are not set, issue will be created using default token.

* `template` (required) - `template` key or `type` from `templatesWithIds` (they are the same)
* `files`: array of:
  * `Buffer` instance
  * `object` with such fields:
    * `file`: `Buffer`, required
    * `fileName`: name of the file
    * `knownLength`: length for formData field
    * `contentType`: file's content-type (ex. image/png)
* `projectId` (required) - project ID to create issue in
* `creationSettings` - [IssuesCreationSettings](IssuesCreationSettings.md). overrides template creation settings. Please
  be careful with this option: any checks are disabled on this one. The only parameter that does not override template
  creations settings is labels - they are simply added to existing labels. You can also pass other API params here

## processWebhook

Closes issue, if it matches three conditions:

* It belongs to one of `projects` from package settings
* It does not include any `labels` from package settings
* It was just opened or reopened

### Options

* `webhookEvent` - first function argument, object. For example: `req.body`

### Returns

* `event` - `issue-closed` or `issue-unknown`
    * `issue-closed` - issue has been automatically closed, using `closeIssue` function and with `closeComment`
    * `issue-uknown` - no action was made. You simply receive webhook event for your purposes
* `issue` - `IGICWebhookIssue`, matching Gitlab issue
* `canBeClosed` - you can call `closeIssue` function on this issue manually if you want to
* `isClosed` - does this issue currently closed. You can also get this information manually from `issue` field

# Gitlab API functions:

## gitlabRequest

Allows you to make a request to Gitlab API.

### Options (object)

* `path` (required) - path (without slash) for request. Example: `user`
* `method` (required) - request METHOD
* `apiVersion` - API version to make request with. Default: `v4`
* `personalAccessToken` - token to make requests with. Ignored if `oAuthAccessToken` exists.
  Default: `personalAccessToken` from package options
* `oAuthAccessToken` - oAuth token for request.
* `headers` - custom headers for request
* `raw` - if `true`, will return `got` library object with additional information instead of JSON body

#### Request options (exactly ONE of them required)

* `query` - an object with both `strings` key-value or `URLSearchParams`
* `body` - object. Will set `content-type` to `application/json`
* `form-data` - an object from `form-data` package

### Example

```typescript
const gitlabUser = await gic.gitlabRequest<{ id: number, username: string, avatar_url: string }>({
  path: 'user',
  method: 'GET',
  oAuthAccessToken: user.accessToken,
});
```

## getUserIdByNickname

### Options

* `nickname` (required) - string

### Returns

* `ID` of user (`number`) or `null`

## getIssues, getIssuesToClose

While getIssues simply searches for issues, getIssuesToClose allows you to search issues matching close condition to
close them manually using `closeIssue` function

### Options (object)

Please see Gitlab API documentation for more information about those fields

```typescript
//both getIssues and getIssuesToClose options
interface GICIGitlabSearchParams {
  projectId: number;
  page: number;
  per_page?: number;
  assigneeId?: number | 'None' | 'Any';
  authorId?: number;
  confidential?: boolean;
  created_after?: string | Date;
  created_before?: string | Date;
  updated_after?: string | Date;
  updated_before?: string | Date;
  due_date?: 0 | 'overdue' | 'week' | 'month' | 'next_month_and_previous_two_weeks';
  iids?: number[];
  milestone?: string | 'Any' | 'None';
  search?: string;
  weight?: number;
}

//getIssues only, all optional
type GICGitlabSearchParams = GICIGitlabSearchParams & Partial<{
  labels: string[] | 'None' | 'All';
  non_archived: boolean;
  not: string;
  order_by: 'created_at' | 'updated_at' | 'priority' | 'due_date' | 'relative_position' | 'label_priority' | 'milestone_due' | 'popularity' | 'weight';
  scope: 'created_by_me' | 'assigned_to_me' | 'all';
  sort: 'asc' | 'desc';
  state: 'opened' | 'closed';
}>;
```

### Returns

```typescript
interface GICIGitlabSearchResult {
  perPage: number;
  issues: GICGitlabIssue[]; //matches Gitlab fields
  totalIssues: number;
  totalPages: number;
}
```

## getProjectActiveMilestones

### Options

* `projectId` - number

### Returns

An `array` of `IGICGitlabMilestone`, matching `Gitlab` fields

## getLatestProjectMilestone

### Options

* `projectId` - number

### Returns

An `IGICGitlabMilestone`, matching `Gitlab` fields, or `null`

## closeIssue

Closes issue and leaves `closeComment` in it

### Options (object)

* `projectId` - number
* `issueId` - number, issue's `iid`

## createComment

### Options (object)

* `projectId` - number
* `issueId` - number, issue's `iid`
* `comment` - string, option, default: `closeComment`

## oAuth

OAuth methods belongs to separate section of library. Please view [oAuth.md](oAuth.md) for more information.
