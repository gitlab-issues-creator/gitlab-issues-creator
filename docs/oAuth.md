# oAuth methods

## Configuration

oAuth methods, just like most other methods, are placed in main class. In order to use them, you must simply
call `gic.oAuth(options)`. Also, you must create an [Application](https://gitlab.com/-/profile/applications) first

### Options

* `clientId` (required, string) - client ID of your application
* `clientSecret` (required, string) - client Secret of your application
* `redirectUrl` (required, string) - default Redirect URL of your application,
  example: `https://api.example.com/gitlab/callback`
* `scopes` (required) - an array
  of [Personal Access Token scopes](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#limiting-scopes-of-a-personal-access-token)

### Example

```typescript
const gicOAuth = gic.oAuth({
  clientId: APP_ID,
  clientSecret: APP_SECRET,
  redirectUrl: APP_REDIRECT_URL,
  authScopes: ['api'],
});
```

## Methods

### convertAuthScopesToGitlabFormat

Transforms your scopes to Gitlab format. For example, array `['read_repository', 'read_user']` will be converted
to `read_repository+read_user`

#### Returns

`string`

### getRedirectURL

Returns url for you to redirect user on.

#### Options (object)

* `redirectUrl` - Redirect URL `string`. Default: `redirectUrl` from oAuth initial settings
* `state` - State `string` to return in `query` after auth. Don't worry, library will convert it to url params.
* `returnURLObject` - by default, function will return you a simple `string`. But if you set this option to `true`, it
  will return [URL](https://developer.mozilla.org/ru/docs/Web/API/URL)

#### Returns

`string`, if `returnURLObject` option is `undefined` or `false`,
or [URL](https://developer.mozilla.org/ru/docs/Web/API/URL), if this options is `true`.

### processAuthCallback

#### Options (object)

* `code` (required) - `string` code param from `query`
* `redirectUrl` - Redirect URL `string`  you used on user's authorization. Default: `redirectUrl` from oAuth initial
  settings

#### Returns

`null` in very unlucky case of failed authorization (it will throw an Error in case of invalid or expired token tho), or
an `object` with fields:

* `tokenResponse`: an object matching Gitlab fields (example is below)

```typescript
interface GICIOauthTokenResponse {
  access_token: string;
  token_type: string;
  expires_in: number;
  refresh_token: string;
  created_at: number;
}
```

* `user`: `GICGitlabUser`, matching Gitlab fields;

### receiveAccessToken

#### Options (object)

* `code` (required) - `string` code param from `query`
* `redirectUrl` - Redirect URL `string`  you used on user's authorization. Default: `redirectUrl` from oAuth initial
  settings

#### Returns

Gitlab response, `GICIOauthTokenResponse`

### refreshAccessToken

#### Options (object)

* `refreshToken` (required) - `string` user's refresh token, that you saved previously
* `redirectUrl` - Redirect URL `string` you used on user's authorization. Default: `redirectUrl` from oAuth initial
  settings

#### Returns

Gitlab response, `GICIOauthTokenResponse`
