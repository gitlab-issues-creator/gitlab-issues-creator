# Examples

## Product

You can see complete, tested, in-product usage
here: [Use Case Example](https://gitlab.com/gitlab-issues-creator/use-case-example)

* [Initialization](https://gitlab.com/gitlab-issues-creator/use-case-example/-/blob/master/api/helpers/gitlab.ts)
* [oAuth and webhooks](https://gitlab.com/gitlab-issues-creator/use-case-example/-/blob/master/api/views/gitlab.ts)
* [Issues creation](https://gitlab.com/gitlab-issues-creator/use-case-example/-/blob/master/api/views/issues.ts)
* [Templates describe](https://gitlab.com/gitlab-issues-creator/use-case-example/-/blob/master/api/helpers/templates.ts)

## Plain

### Work with webhooks

```typescript
const webhookResult = await gic.processWebhook(req.body)
```

### Issue creation

```typescript
const issue = await gic.createIssue({
  template: 'test',
  projectId,
  title: 'This is test title',
  description: 'This is default description.\n[file1]',
  oAuthAccessToken: user.oauthtoken,
  files: [
    readFileSync(file.path),
    readFileSync('assets/bug.png'),
  ],
})
```

### Issues search

```typescript
const result = await main.getIssues({
      page: 1,
      projectId,
      labels: ['test1', 'test2'],
      non_archived: true,
      order_by: 'due_date',
      scope: 'all',
      sort: 'asc',
      state: 'closed',
      per_page: 10000,
      assigneeId: 'None',
      confidential: true,
      created_after: new Date(),
      due_date: 0,
      milestone: 'Any',
});
```
