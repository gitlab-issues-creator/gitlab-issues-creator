# Configuration

To import package, simply type:

```typescript
import GICTemplate from 'gitlab-issues-creator';
```

or:

```javascript
const GICTemplate = require('gitlab-issues-creator')
```

## Package options

* `entrypoint` - allows to set custom entrypoint for API requests. Default is `https://gitlab.com`.
* `personalAccessToken` (required) - your [access token](https://gitlab.com/-/profile/personal_access_tokens) with api
  permission. It will be used to edit, close and comment issues.
* `labels` (at least one label required) - an `array` of `string` labels. Each created issue should have one of those
  labels, and issues with at least one label of this list will not be closed. Please specify as first label your primary
  label: if no valid labels will be defined in template, first label will be chosen.
* `projects` (at least one project ID required) - an `array` of `number` projects ids. You can find project ID on
  project's Gitlab page. You will only be able to create issues within those projects. Also, webhook will only close
  issues in those projects.
* `templates` (at least one template required) - an `object` of templates. You can find template options in Template
  section below.
* `closeMessage` (required) - the message to close issues with (Markdown supported). Can be `string` or `function`.
  Function accepts `projectId`, which is number, as first parameter and must return string.
* `debug` - debug mode settings. Default is `false`. If you set this option to `true`, it will print nice-formatted
  errors in console.
    * This parameter can also be an object. Object structure:
        * `enabled` - `boolean` or `function`. Function accepts first parameter `error`, which is `string` or `Error`.
          Function will receive an unformatted log.
        * `logLevels` - an array of log levels (`info`, `warn`, `error`, `all`), or string `all`
* `defaultAssigneesIds` - an array of `numbers` users ids to assign on **all** templates

## Templates

Below you can find template options

* Parameters to use on frontend
    * `title` (required) - the title you will display on frontend-side
    * `description` - the description you want to display on frontend-side
    * `defaultText` (required) - default text for this template type you want to use on frontend-side
* Parameters for issue creation: any parameters from [IssuesCreationSettings](IssuesCreationSettings.md)

## Example

### Typescript:
```typescript
import GICTemplate, { GICTemplateList } from 'gitlab-issues-creator';

//You will need this for strong typing
type ITemplatesList = 'vulnerability' | 'bug' | 'feature-request';

//Generic argument is not required
const templates: GICTemplateList<ITemplatesList> = {
  vulnerability: {
    title: 'Vulnerability',
    description: 'If you want to report a critical bug. Created issue will be private',
    defaultText: `- [x] Creating this issue I have tried to look for duplicates and didn't find one
- [x] Creating this issue I assume bug does exist

## Issue description

Please describe your issue as detailed as possible`,
    prependTitle: 'Vulnerability: ',
    labelsToAssign: ['priority::critical', 'vulnerability'],
    weight: 5,
    confidential: true,
  },
  //...
}

const gic = new GICTemplate({
  personalAccessToken: process.env.token,
  labels: ['approved'],
  projects: [22162284, 22161981],
  defaultAssigneesIds: [2537151],
  closeMessage: (projectId: number) => {
    const project: 'main' | 'test' = projectId === 22161981 ? 'main' : 'test';
    const link = `https://gitlab.dk4000.pro/project/${ project }`;
    return `Your issue has been closed. Please report your issue on [dedicated form](${ link }) using a template.`;
  },
  templates,
});
```

### Javascript:
```javascript
const GICTemplate = require('gitlab-issues-creator');

const templates = {
  vulnerability: {
    title: 'Vulnerability',
    description: 'If you want to report a critical bug. Created issue will be private',
    defaultText: `- [x] Creating this issue I have tried to look for duplicates and didn't find one
- [x] Creating this issue I assume bug does exist

## Issue description

Please describe your issue as detailed as possible`,
    prependTitle: 'Vulnerability: ',
    labelsToAssign: ['priority::critical', 'vulnerability'],
    weight: 5,
    confidential: true,
  },
  //...
}

const gic = new GICTemplate({
  personalAccessToken: process.env.token,
  labels: ['approved'],
  projects: [22162284, 22161981],
  defaultAssigneesIds: [2537151],
  closeMessage: (projectId) => {
    const project: 'main' | 'test' = projectId === 22161981 ? 'main' : 'test';
    const link = `https://gitlab.dk4000.pro/project/${ project }`;
    return `Your issue has been closed. Please report your issue on [dedicated form](${ link }) using a template.`;
  },
  templates,
});
```
