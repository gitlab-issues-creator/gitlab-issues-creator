# Issues Creation Settings

Those settings are used to create issues

* `prependTitle` - text to be appended **before** issue title. For example, if you type `Feature Request: ` here, every
  issue title with this template type will start with that text
* `assigneesIds` - users to assign to every issue
* `overwriteDefaultAssignees` - allows to ignore `defaultAssigneesIds` from package options
* `labelsToAssign` - labels to assign to every issue
* `confidential` - allows you to make every created issue confidential. It will be visible only for privileged users and
  issue author
* `dueDate` - due date of issue. Can be JS Date or string with format `YYYY-MM-DD`
* `epicId` - allows you to set epic for all issues. This is the global ID, used in API
* `epicIid` - allows you to set epic for all issues. This is the IID inside the project
* `milestoneId` - can be `number` ID or word `latest`. `latest` will assign all issues to **first** created active
  milestone
* `weight` - allows assigning weight to all issues
