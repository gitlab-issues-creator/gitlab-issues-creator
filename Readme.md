[![pipeline status](https://gitlab.com/gitlab-issues-creator/gitlab-issues-creator/badges/master/pipeline.svg)](https://gitlab.com/gitlab-issues-creator/gitlab-issues-creator/-/commits/master)
[![coverage report](https://gitlab.com/gitlab-issues-creator/gitlab-issues-creator/badges/master/coverage.svg)](https://gitlab.com/gitlab-issues-creator/gitlab-issues-creator/-/commits/master)
[![downloads](https://badgen.net/npm/dt/gitlab-issues-creator)](https://www.npmjs.com/package/gitlab-issues-creator)
[![downloads](https://badgen.net/npm/license/gitlab-issues-creator)](LICENSE)
[![npm version](https://badgen.net/npm/v/gitlab-issues-creator)](https://www.npmjs.com/package/gitlab-issues-creator)

# Requirements
This library requires Node 14.13.1 or higher or Node 16 or higher

# About

## Why

This package actually allows you to do such things:

* Work with issues-related webhooks
* Authorize users using oAuth
* Forbid to create issues without templates (sadly, Gitlab allows to do that)
* Create much more useful templates to create issues programmatically
* Automatically assign labels etc to issues even if users don't have rights to do that: issue can be created using
  user's oAuth token, then it's being edited using **your** token
* Make API calls to Gitlab with a nice format and Typescript support
* A little more, see `Usage` section of docs

## Notice

You will get much better experience using Typescript. Methods frequently has so many options easy to break, Typescript
will provide you great validation for all of this.

Package uses `got` library to make requests. It has bad default errors messages IMO, you can enable `debug` mode or take
a look on `src/classes/debug.class.ts`

# Documentation

## Installation

* `npm i --save gitlab-issues-creator`

or
* `yarn add gitlab-issues-creator`

## [Configuration](https://gitlab.com/gitlab-issues-creator/gitlab-issues-creator/-/blob/master/docs/Configuration.md)

## Usage

### [Working with API methods](https://gitlab.com/gitlab-issues-creator/gitlab-issues-creator/-/blob/master/docs/Methods.md)

### [Working with oAuth](https://gitlab.com/gitlab-issues-creator/gitlab-issues-creator/-/blob/master/docs/oAuth.md)

### [Examples](https://gitlab.com/gitlab-issues-creator/gitlab-issues-creator/-/blob/master/docs/Examples.md)
