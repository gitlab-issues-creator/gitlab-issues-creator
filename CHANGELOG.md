# 2.0.0
* Drop Node < 14.13.1 support
* Change module to ES2020
* Migrate to Yarn
* Update deps

# 1.1.9
* Some fields of `creationSettings` didn't override `template`'s options

# 1.1.8
* Fix confidential issues are not accepted

# 1.1.7
* Allow to specify filename, content type and known length of creation `files`

# 1.1.5
* Reopen issue if it was closed after it's edit

# 1.1.3
Allow strongly typing of template

# 1.1.2
* Labels from settings are no longer included: only first label is included. If valid labels are present in template, this setting is ignored.

# 1.1.0

* Add oAuth class
* Add oAuth to main class as getter
* Rename oauth to oAuth everywhere
* Rename Callbacks to Webhooks

# 1.0.0

* Initial release
