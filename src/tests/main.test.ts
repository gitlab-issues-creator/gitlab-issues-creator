import { GICTemplate } from '../classes/main.class';
import { config } from 'dotenv';

config();

describe('main', () => {
  const main = new GICTemplate({
    personalAccessToken: process.env.PERSONAL_NPM_TOKEN || '',
    labels: ['label'],
    projects: [22162284],
    templates: {
      test: {
        title: 'Test',
        defaultText: 'Default',
      },
    },
    closeMessage: 'Close message',
  });

  test('getter', () => {
    expect(JSON.stringify(main.templatesWithIds)).toBe(JSON.stringify([
      {
        id: 1,
        type: 'test',
        title: 'Test',
        defaultText: 'Default',
      },
    ]));
  });
});
