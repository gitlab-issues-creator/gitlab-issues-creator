import { GICTemplate } from '../classes/main.class';
import { readFileSync } from 'fs';
import { GICDebug } from '../classes/debug.class';
import { config } from 'dotenv';
import { dirname } from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

config();

describe('Various Gitlab functions tests', () => {
  const projectId = 22162284;
  const main = new GICTemplate({
    personalAccessToken: process.env.PERSONAL_NPM_TOKEN || '',
    labels: ['approved'],
    projects: [projectId],
    templates: {
      test: {
        title: 'Test',
        defaultText: 'Default',
        labelsToAssign: ['incident'],
        prependTitle: `This is prepended title ${ Math.random() } for issue created on ${ Date.now() }: `,
        assigneesIds: [2537151],
        confidential: true,
        dueDate: new Date(2020, 10, 10),
        milestoneId: 'latest',
        weight: 5,
      },
    },
    closeMessage: 'Close message',
  });

  test('Get user id by nickname', async () => {
    expect(await main.getUserIdByNickname('daniluk4000')).toBe(2537151);
  }, 15000);

  test('Get user id by nickname (fail)', async () => {
    expect(await main.getUserIdByNickname('daniluk400000000')).toBe(null);
  }, 15000);

  test('Get latest milestone', async () => {
    expect((await main.getLatestProjectMilestone(projectId))?.title).toBe('test3 - open and group');
  }, 15000);

  test('User issue creation (fail template)', async () => {
    try {
      await main.createIssue({
        // @ts-ignore
        template: undefined,
      });
      expect(true).toBe(false);
    }
    catch (e) {

    }
  });

  test('User issue creation (fail project)', async () => {
    try {
      //@ts-ignore
      await main.createIssue({
        template: 'test',
        projectId: 0,
      });
      expect(true).toBe(false);
    }
    catch (e) {

    }
  });

  test('User issue creation', async () => {
    try {
      const issue = await main.createIssue({
        template: 'test',
        projectId,
        title: 'This is test title',
        description: 'This is default description.\n[file1]',
        personalAccessToken: process.env.PERSONAL_TEMP_TOKEN || '',
        files: [
          readFileSync(`${ __dirname }/../assets/test-image.jpeg`),
          readFileSync(`${ __dirname }/../assets/test-image.jpeg`),
        ],
      });

      await main.closeIssue({
        projectId,
        issueId: issue.iid,
      });

      expect(typeof issue.iid).toBe('number');
      expect(issue.labels.includes('approved')).toBe(true);
      expect(issue.labels.includes('incident')).toBe(true);
      expect(issue.milestone?.title).toBe('test3 - open and group');
      expect(issue.assignees.some(x => x.username === 'daniluk4000')).toBe(true);
      expect(issue.author.username !== 'daniluk4000').toBe(true);
      expect(issue.weight).toBe(5);
      expect(issue.confidential).toBe(true);
    }
    catch (e) {
      new GICDebug(true).processError(e, 'test');
    }
  }, 15000);

  test('Issues search', async () => {
    const result = await main.getIssues({
      page: 1,
      projectId,
      labels: ['test1', 'test2'],
      non_archived: true,
      order_by: 'due_date',
      scope: 'all',
      sort: 'asc',
      state: 'closed',
      per_page: 10000,
      assigneeId: 'None',
      confidential: true,
      created_after: new Date(1),
      due_date: 0,
      iids: Array.from(new Array(10).keys()),
      milestone: 'Any',
    });

    expect(result.perPage).toBe(100);
    expect(typeof result.totalIssues).toBe('number');
    expect(result.totalPages > 0).toBe(true);
    expect(typeof result.issues).toBe('object');
  }, 15000);

  test('Issues search (invalud values)', async () => {
    try {
      await main.getIssues({
        page: 1,
        projectId,
        labels: ['test1', 'test2'],
        non_archived: true,
        order_by: 'due_date',
        scope: 'all',
        sort: 'asc',
        state: 'closed',
        per_page: 10000,
        assigneeId: 'None',
        confidential: true,
        created_after: new Date(1),
        due_date: 0,
        // @ts-ignore
        iids: Array.from(new Array(10).keys()).map(x => x.toString()),
        milestone: 'Any',
      });

      expect(false).toBe(true);
    }
    catch (e) {

    }
  }, 15000);

  test('Unclosed search', async () => {
    const result = await main.getIssuesToClose({
      page: 2,
      projectId,
      per_page: 25,
      assigneeId: 'None',
      confidential: true,
      created_after: new Date(1),
      due_date: 0,
      iids: Array.from(new Array(10).keys()),
      milestone: 'Any',
    });

    expect(result.perPage).toBe(25);
    expect(typeof result.totalIssues).toBe('number');
    expect(result.totalPages > 0).toBe(true);
    expect(typeof result.issues).toBe('object');
  }, 15000);
});
