import { GICTemplate } from '../classes/main.class';
import { config } from 'dotenv';

config();

describe('Webhook', () => {
  const main = new GICTemplate({
    personalAccessToken: process.env.PERSONAL_NPM_TOKEN || '',
    labels: ['label'],
    projects: [22162284],
    templates: {
      test: {
        title: 'Test',
        defaultText: 'Default',
        overwriteDefaultAssignees: true,
        assigneesIds: [2537151],
      },
    },
    closeMessage: 'Close message',
  });

  const main2 = new GICTemplate({
    personalAccessToken: process.env.PERSONAL_NPM_TOKEN || '',
    labels: ['label'],
    projects: [22162284],
    defaultAssigneesIds: [2537151],
    templates: {
      test: {
        title: 'Test',
        defaultText: 'Default',
      },
    },
    closeMessage: (projectId: number) => 'Close message',
  });

  test('processWebhookUnknownType', async () => {
    expect(await main.processWebhook({
      object_kind: 'other',
      event_type: 'other',
      project: {
        id: 22162284,
        name: 'Project',
      },
      object_attributes: {},
    })).toBe(null);
  });

  test('processWebhookUnknownProject', async () => {
    expect(await main.processWebhook({
      object_kind: 'other',
      event_type: 'other',
      project: {
        id: 0,
        name: 'Project',
      },
      object_attributes: {},
    })).toBe(null);
  });

  test('processWebhookClose', async () => {
    expect((await main.processWebhook({
      object_kind: 'issue',
      event_type: 'issue',
      project: {
        id: 22162284,
        name: 'Project',
      },
      object_attributes: {
        id: 0,
        iid: 2,
        project_id: 22162284,
        labels: [
          {
            id: 0,
            title: 'label',
          },
        ],
        action: 'close',
      },
    }))?.event).toBe('issue-unknown');
  });

  test('processWebhookOpen', async () => {
    expect((await main.processWebhook({
      object_kind: 'issue',
      event_type: 'issue',
      project: {
        id: 22162284,
        name: 'Project',
      },
      object_attributes: {
        id: 0,
        iid: 2,
        project_id: 22162284,
        labels: [
          {
            id: 0,
            title: 'label',
          },
        ],
        action: 'open',
      },
    }))?.event).toBe('issue-unknown');
  });

  test('processWebhookOpenFail', async () => {
    expect((await main.processWebhook({
      object_kind: 'issue',
      event_type: 'issue',
      project: {
        id: 22162284,
        name: 'Project',
      },
      object_attributes: {
        id: 0,
        iid: 2,
        project_id: 22162284,
        labels: [
          {
            id: 0,
            title: 'nope',
          },
        ],
        action: 'open',
      },
    }))?.event).toBe('issue-closed');
  });

  test('processWebhookOpenFailFunctionCloseMessage', async () => {
    expect((await main2.processWebhook({
      object_kind: 'issue',
      event_type: 'issue',
      project: {
        id: 22162284,
        name: 'Project',
      },
      object_attributes: {
        id: 0,
        iid: 2,
        project_id: 22162284,
        labels: [
          {
            id: 0,
            title: 'nope',
          },
        ],
        action: 'open',
      },
    }))?.event).toBe('issue-closed');
  });
});
