import { GICDebug } from '../classes/debug.class';
import { config } from 'dotenv';
import { RequestError } from 'got';

config();

describe('debug', () => {
  test('infoConsoleLog', () => {
    const debug = new GICDebug(true);
    debug.sendLog({
      prefix: 'test',
      message: 'string',
      severity: 'info',
    });
  });

  test('infoFunction', (done) => {
    const debug = new GICDebug({
      enabled: () => {
        done();
      },
      logLevels: ['info'],
    });

    debug.sendLog({
      prefix: 'test',
      message: 'string',
      severity: 'info',
    });
  });

  test('errorConsoleLog', () => {
    try {
      const debug = new GICDebug({
        enabled: true,
        logLevels: ['error'],
      });
      debug.processError(new Error('test'), 'test');
    }
    catch (e) {
      expect((e as Error).message).toBe('test');
    }
  });

  test('errorFunction', (done) => {
    try {
      const debug = new GICDebug({
        enabled: () => {
          done();
        },
        logLevels: 'all',
      });
      // @ts-ignore
      debug.processError(new RequestError('test', new Error(), {}), 'test');
    }
    catch (e) {

    }
  });
});
