export const DEFAULT_ENTRYPOINT = 'https://gitlab.com';

function escapeRegExp(string: string) {
  return string.replace(/[.*+\-?^${}()|[\]\\]/g, '\\$&');
}

export function replaceAll(text: string, search: string, replace: string): string {
  return text.replace(new RegExp(escapeRegExp(search), 'g'), replace);
}
