export function checkValidDate(date: any): boolean {
  return date instanceof Date && !isNaN(date.valueOf());
}

export function convertDateToGitlabFormat(date: Date): string {
  return `${ date.getFullYear() }-${ ('0' + (date.getMonth() + 1)).slice(-2) }-${ ('0' + date.getDate()).slice(-2) }`;
}
