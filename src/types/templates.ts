export interface GICTemplate extends GICTemplateCreationInfo {
  title: string
  description?: string | null
  defaultText: string
}

export type GICTemplateList<T extends string = string> = Record<T, GICTemplate>
export type GICTemplateFrontendList<T extends GICTemplateList> = Array<{ id: number, type: keyof T } & GICTemplate>

export type GICTemplateCreationInfo = Partial<{
  prependTitle: string
  assigneesIds: number[]
  overwriteDefaultAssignees: boolean
  labelsToAssign: string[]
  confidential: boolean
  /**
   * @description if string, format has to match YYYY-MM-DD
   */
  dueDate: Date | string
  epicId: number
  epicIid: number
  milestoneId: number | 'latest'
  weight: number
}>

export interface GICTemplateCreationDataFile {
  file: Buffer
  knownLength?: number;
  fileName?: string;
  contentType?: string;
}

export interface GICTemplateCreationData<T extends GICTemplateList> {
  title: string
  /**
   * @description this field accepts markdown format and is used along with files parameter. You can reference files by
   *   their index with format [fileINDEX], for example, [file0]. If file with such index does not exist in files, it
   *   will be skipped. If you will not reference all files in description, they will be placed in the bottom of
   *   description with original order.
   */
  description: string
  personalAccessToken?: string
  oAuthAccessToken?: string
  template: keyof T
  files?: Array<GICTemplateCreationDataFile | Buffer>
  projectId: number
  /**
   * @description overrides template creation settings. Please be careful with this option: any checks are disabled on
   *   this one. The only parameter that does not override template creations settings is labels. You can also pass
   *   other API params here
   */
  creationSettings?: GICTemplateCreationInfo & Record<string, any>
}
