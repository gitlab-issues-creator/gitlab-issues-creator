import { GICTemplateList } from './templates';
import { GICILogFunctionSetting, GICILogSeverityLogLevels } from './log';

export interface GICMainClassSettings<T extends GICTemplateList> {
  /**
   * @description Entrypoint for API requests. Ex.: https://gitlab.example.com
   * @default https://gitlab.com
   */
  entrypoint?: string
  /**
   * @description Personal access token of user who will close and open issues
   */
  personalAccessToken: string
  /**
   * @description Names of labels to check and not close issue if one of them exists (at least one is required)
   */
  labels: [string, ...string[]]
  /**
   * @description Ids of projects which issues to watch (at least one is required)
   */
  projects: [number, ...number[]]
  /**
   * @description Issues templates
   */
  templates: T
  /**
   * @description Message issues will be closed with. Markdown supported
   */
  closeMessage: string | ((projectId: number) => string)
  /**
   * @description Enables extended logging. If true, sets log level to error
   * @default false
   */
  debug?: {
    /**
     * If true, sets log level to error
     */
    enabled: boolean | GICILogFunctionSetting
    /**
     * @default error
     */
    logLevels?: GICILogSeverityLogLevels
  } | boolean
  /**
   * @description Describes default assignees for all templates. Can be overwritten using overwriteDefaultAssignees in
   *   template settings
   */
  defaultAssigneesIds?: number[]
}

