import { GICGitlabIssue } from './types';

export interface GICIGitlabSearchParams {
  projectId: number
  page: number

  per_page?: number
  assigneeId?: number | 'None' | 'Any'
  authorId?: number
  confidential?: boolean
  created_after?: string | Date
  created_before?: string | Date
  updated_after?: string | Date
  updated_before?: string | Date
  due_date?: 0 | 'overdue' | 'week' | 'month' | 'next_month_and_previous_two_weeks'
  iids?: number[]
  milestone?: string | 'Any' | 'None'
  search?: string
  weight?: number
}

export type GICGitlabSearchParams = GICIGitlabSearchParams & Partial<{
  labels: string[] | 'None' | 'All'
  non_archived: boolean
  /**
   * @description Hash param
   * @see {@link https://docs.gitlab.com/ee/api/issues.html#list-issues|List Issues}
   */
  not: string
  /**
   * @default created_at
   */
  order_by: 'created_at' | 'updated_at' | 'priority' | 'due_date' | 'relative_position' | 'label_priority' | 'milestone_due' | 'popularity' | 'weight'
  scope: 'created_by_me' | 'assigned_to_me' | 'all'
  /**
   * @default desc
   */
  sort: 'asc' | 'desc'
  state: 'opened' | 'closed'
}>

export interface GICIGitlabSearchResult {
  perPage: number
  issues: GICGitlabIssue[]
  totalIssues: number
  totalPages: number
}

