export interface GICGitlabIssue {
  id: number
  iid: number
  project_id: number
  title: string
  description: string
  state: string
  created_at: string
  updated_at: string | null
  closed_at: string | null
  closed_by: GICGitlabUser | null
  labels: string[]
  milestone: IGICGitlabMilestone | null
  assignees: GICGitlabUser[]
  author: GICGitlabUser
  assignee: GICGitlabUser | null
  user_notes_count: number
  merge_requests_count: number
  upvotes: number
  downvotes: number
  due_date: string | null
  confidential: boolean
  discussion_locked: boolean | null
  web_url: string
  time_stats: { [key: string]: any }
  task_completion_status: { [key: string]: any }
  weight?: number | null
  _links: {
    self: string
    notes?: string
    award_emoji?: string
    project: string
    [key: string]: any
  }

  [key: string]: any
}

export interface GICGitlabUser {
  id: number
  name: string
  username: string
  avatar_url: string
  web_url: string

  [key: string]: any
}

export interface IGICWebhookEvent {
  object_kind: string
  event_type: string
  project: {
    id: number
    name: string
    [key: string]: any
  }
  object_attributes: Record<string, any>

  [key: string]: any
}

export interface IGICWebhookIssue extends IGICWebhookEvent {
  object_kind: 'issue'
  event_type: 'issue'
  user: Omit<GICGitlabUser, 'id' | 'state'> & {
    avatar_url: string
    email: string
  }
  object_attributes: {
    id: number
    iid: number
    title: string
    project_id: number
    action: 'reopen' | 'open' | string
    state: 'opened' | 'closed' | string
    labels: {
      id: number
      title: string
    }[]
    url: string
    [key: string]: any
  }
}

export interface IGICGitlabMilestone {
  id: number
  iid: number
  project_id: number
  title: string
  state: 'active' | 'closed' | string
  expired: boolean
  created_at: string

  [key: string]: any
}

export interface IGICGitlabFile {
  alt: string
  url: string
  full_path: string
  markdown: string

  [key: string]: any
}
