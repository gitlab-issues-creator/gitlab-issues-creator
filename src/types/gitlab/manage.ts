export interface GICIGitlabIssueAction {
  projectId: number
  issueId: number
}

export interface GICIGitlabCreateComment extends GICIGitlabIssueAction {
  comment?: string
}
