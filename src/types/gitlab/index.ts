import { GICTemplateList } from '../templates';
import { GICMainClassSettings } from '../main';
import { Method } from 'got';
import FormData from 'form-data';
import { URLSearchParams } from 'url';

export interface GICIGitlabMainClassSettings<T extends GICTemplateList> extends GICMainClassSettings<T> {
  entrypoint: string
}

interface GICIGitlabRequestOptionsDraft {
  path: string
  /**
   * @default v4
   */
  apiVersion?: string
  personalAccessToken?: string
  oAuthAccessToken?: string
  headers?: Record<string, string>
}

interface GICIGitlabRequestOptionsQuery extends GICIGitlabRequestOptionsDraft {
  method: Method,
  query?: Record<string, string> | URLSearchParams
  body?: never
  form?: never
}

interface GICIGitlabRequestOptionsBody extends GICIGitlabRequestOptionsDraft {
  method: Method,
  query?: never
  body?: Record<string, any>
  form?: never
}

interface GICIGitlabRequestOptionsForm extends GICIGitlabRequestOptionsDraft {
  method: Exclude<Method, 'GET' | 'OPTIONS' | 'get' | 'options'>,
  query?: never
  body?: never
  form?: FormData
}

export type GICIGitlabRequestOptions =
  GICIGitlabRequestOptionsQuery
  | GICIGitlabRequestOptionsBody
  | GICIGitlabRequestOptionsForm
