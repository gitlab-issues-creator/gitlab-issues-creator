import { IGICWebhookIssue } from './types';

interface GICWebhookReturnIssueCreated {
  event: 'issue-closed'
  issue: IGICWebhookIssue
  canBeClosed: true
  isClosed: true
}

interface GICWebhookReturnIssueUnknown {
  event: 'issue-unknown'
  issue: IGICWebhookIssue
  canBeClosed: boolean
  isClosed: boolean
}

export type GICWebhookReturnIssue = GICWebhookReturnIssueCreated | GICWebhookReturnIssueUnknown | null
