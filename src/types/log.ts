export interface GICILogFunctionSettings {
  prefix: string,
  message: string | Error,
  severity: GICILogSeverity
}

export type GICILogSeverity = 'info' | 'error' | 'warn'
export type GICILogSeverityLogLevels = (GICILogSeverity | 'all')[] | 'all'

export type GICILogFunctionSetting = (log: string | Error) => any
