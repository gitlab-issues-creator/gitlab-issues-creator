import { GICTemplateList } from './templates';
import { GICIGitlabMainClassSettings } from './gitlab';

export type GICOAuthClassSettings<T extends GICTemplateList> = GICIGitlabMainClassSettings<T> & GICIOAuthClassSettings

export interface GICIOAuthClassSettings {
  clientId: string
  clientSecret: string
  redirectUrl: string
  /**
   * @see {@link https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#limiting-scopes-of-a-personal-access-token|Personal Access Tokens}
   */
  authScopes: GICIOAuthScopes[]
}

export type GICIOAuthScopes =
  'read_user'
  | 'api'
  | 'read_api'
  | 'read_registry'
  | 'write_registry'
  | 'sudo'
  | 'read_repository'
  | 'write_repository'

export interface GICIOAuthURLSettings {
  redirectUrl: string,
  state: string,
  returnURLObject: boolean
}

export interface GICIOauthTokenResponse {
  access_token: string
  token_type: string
  expires_in: number
  refresh_token: string
  created_at: number
}
