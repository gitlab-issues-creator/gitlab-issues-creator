import { GICTemplateList } from '../types/templates';
import { IGICWebhookEvent, IGICWebhookIssue } from '../types/gitlab/types';
import { GICGitlab } from './gitlab.class';
import { GICIGitlabMainClassSettings } from '../types/gitlab';
import { GICWebhookReturnIssue } from '../types/gitlab/webhook';

export class GICWebhook<T extends GICTemplateList> extends GICGitlab<T> {
  constructor(settings: GICIGitlabMainClassSettings<T>) {
    super(settings);
  }

  async processWebhook(iWebhookEvent: IGICWebhookEvent, ignoredIssues: Set<string>): Promise<GICWebhookReturnIssue> {
    try {
      if (
        typeof iWebhookEvent !== 'object'
        || Array.isArray(iWebhookEvent)
        || (iWebhookEvent.event_type !== 'issue' && iWebhookEvent.event_type !== 'confidential_issue')
        || iWebhookEvent.object_kind !== 'issue'
      ) {
        this.debugger.sendLog({
          prefix: 'webhook',
          message: 'This webhook event is invalid or not issues related, exiting',
          severity: 'info',
        });
        return null;
      }

      // @ts-ignore
      const webhook: IGICWebhookIssue = iWebhookEvent;
      const canBeClosed = this.settings.projects.includes(webhook.project?.id)
        && !webhook.object_attributes.labels?.some(label => this.settings.labels.includes(label.title))
        && !ignoredIssues.has(webhook.object_attributes.title);

      ignoredIssues.delete(webhook.object_attributes.title);

      if (!canBeClosed || (webhook.object_attributes.action !== 'open' && webhook.object_attributes.action !== 'reopen')) return {
        issue: webhook,
        event: 'issue-unknown',
        canBeClosed,
        isClosed: webhook.object_attributes.state === 'closed',
      };

      const issueId = webhook.object_attributes.iid;
      const projectId = webhook.object_attributes.project_id;

      await this.closeIssue({
        issueId,
        projectId,
      });

      return {
        issue: webhook,
        event: 'issue-closed',
        canBeClosed: true,
        isClosed: true,
      };
    }
    catch (e) {
      this.debugger.processError(e, 'webhook');
    }
  }
}
