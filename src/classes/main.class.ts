import { GICTemplateCreationData, GICTemplateFrontendList, GICTemplateList } from '../types/templates';
import { GICMainClassSettings } from '../types/main';
import { IGICWebhookEvent } from '../types/gitlab/types';
import { GICGitlab } from './gitlab.class';
import { GICWebhook } from './webhook.class';
import { DEFAULT_ENTRYPOINT } from '../helpers';
import { GICOAuth } from './oauth.class';
import { GICIOAuthClassSettings } from '../types/oAuth';

export class GICTemplate<T extends GICTemplateList> extends GICGitlab<T> {
  private readonly Webhook: GICWebhook<T>;
  private readonly ignoredIssues: Set<string> = new Set();

  constructor(settings: GICMainClassSettings<T>) {
    super({
      ...settings,
      entrypoint: settings.entrypoint || DEFAULT_ENTRYPOINT,
    });

    this.Webhook = new GICWebhook(this.settings);
  }

  /**
   * @description Converts object of templates with array with additional fields: id, type (type - one of templates
   *   keys). Original order is observed
   */
  get templatesWithIds(): GICTemplateFrontendList<T> {
    const templates: GICTemplateFrontendList<T> = [];

    Object.entries(this.settings.templates).forEach(([key, template], index) => {
      templates[index] = {
        id: index + 1,
        type: key,
        ...template,
      };
    });

    return templates;
  }

  async createIssue(options: GICTemplateCreationData<T>) {
    return this._createIssue(options, this.ignoredIssues);
  }

  processWebhook(webhookEvent: IGICWebhookEvent) {
    return this.Webhook.processWebhook(webhookEvent, this.ignoredIssues);
  }

  oAuth(settings: GICIOAuthClassSettings) {
    return new GICOAuth({
      ...this.settings,
      ...settings,
    });
  }
}
