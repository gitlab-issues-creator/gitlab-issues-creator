import { GICTemplateList } from '../types/templates';
import got from 'got';
import { GICGitlabUser } from '../types/gitlab/types';
import { GICIOAuthScopes, GICIOauthTokenResponse, GICIOAuthURLSettings, GICOAuthClassSettings } from '../types/oAuth';
import { GICDebug } from './debug.class';
import { GICGitlab } from './gitlab.class';

export class GICOAuth<T extends GICTemplateList> {
  settings: GICOAuthClassSettings<T>;
  private readonly authScopes: Set<GICIOAuthScopes>;
  private readonly debugger: GICDebug;
  private readonly gitlab: GICGitlab<T>;

  constructor(settings: GICOAuthClassSettings<T>) {
    if (
      typeof settings.clientId !== 'string'
      || typeof settings.clientSecret !== 'string'
      || typeof settings.redirectUrl !== 'string'
      || typeof settings.authScopes !== 'object'
      || !Array.isArray(settings.authScopes)
    ) throw new Error('oAuth parameters are invalid ');

    this.authScopes = new Set(settings.authScopes);
    this.settings = settings;
    this.debugger = new GICDebug(settings.debug);
    this.gitlab = new GICGitlab(settings);
  }

  convertAuthScopesToGitlabFormat(): string {
    return Array.from(this.authScopes).join('+');
  }

  getRedirectURL(options?: Partial<GICIOAuthURLSettings> & { returnURLObject?: false }): string
  getRedirectURL(options: Partial<GICIOAuthURLSettings> & { returnURLObject: true }): URL
  getRedirectURL(options: Partial<GICIOAuthURLSettings> = {}): URL | string {
    const url = new URL(`${ this.settings.entrypoint }/oauth/authorize`);
    url.searchParams.append('client_id', this.settings.clientId);
    url.searchParams.append('redirect_uri', options.redirectUrl || this.settings.redirectUrl);
    url.searchParams.append('response_type', 'code');
    url.searchParams.append('scope', this.convertAuthScopesToGitlabFormat());
    if (options.state) url.searchParams.append('state', options.state);

    if (options.returnURLObject) return url;
    return url.toString();
  }

  async processAuthCallback(options: { code: string, redirectUrl?: string }): Promise<{ tokenResponse: GICIOauthTokenResponse, user: GICGitlabUser } | null> {
    try {
      const response = await this.receiveAccessToken(options);
      if (!response.access_token) return null;

      const user = await this.gitlab.gitlabRequest<GICGitlabUser>({
        path: 'user',
        method: 'GET',
        oAuthAccessToken: response.access_token,
      });
      if (!user?.id) return null;

      return {
        tokenResponse: response,
        user,
      };
    }
    catch (e) {
      this.debugger.processError(e, 'oauth');
    }
  }

  async receiveAccessToken(options: { code: string, redirectUrl?: string }) {
    return got({
      url: `${ this.settings.entrypoint }/oauth/token`,
      method: 'POST',
      json: {
        client_id: this.settings.clientId,
        client_secret: this.settings.clientSecret,
        grant_type: 'authorization_code',
        redirect_uri: options.redirectUrl || this.settings.redirectUrl,
        code: options.code,
      },
    }).json<GICIOauthTokenResponse>();
  }

  async refreshAccessToken(options: { refreshToken: string, redirectUrl?: string }) {
    return got({
      url: 'https://gitlab.com/oauth/token',
      method: 'POST',
      json: {
        grant_type: 'refresh_token',
        refresh_token: options.refreshToken,
        client_id: this.settings.clientId,
        client_secret: this.settings.clientSecret,
        redirect_uri: options.redirectUrl || this.settings.redirectUrl,
        scope: this.convertAuthScopesToGitlabFormat(),
      },
    }).json<GICIOauthTokenResponse>();
  }
}
