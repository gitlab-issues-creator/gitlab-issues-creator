import { GICTemplateCreationData, GICTemplateCreationDataFile, GICTemplateList } from '../types/templates';
import { GICGitlabSearchParams, GICIGitlabSearchParams, GICIGitlabSearchResult } from '../types/gitlab/search';
import got, { Response } from 'got';
import { GICIGitlabMainClassSettings, GICIGitlabRequestOptions } from '../types/gitlab';
import { GICIGitlabCreateComment, GICIGitlabIssueAction } from '../types/gitlab/manage';
import { GICDebug } from './debug.class';
import { checkValidDate, convertDateToGitlabFormat } from '../helpers/dates';
import { GICGitlabIssue, IGICGitlabFile, IGICGitlabMilestone } from '../types/gitlab/types';
import FormData from 'form-data';
import { URLSearchParams } from 'url';
import { replaceAll } from '../helpers';

export class GICGitlab<T extends GICTemplateList> {
  protected readonly settings: GICIGitlabMainClassSettings<T>;
  protected readonly debugger: GICDebug;

  constructor(settings: GICIGitlabMainClassSettings<T>) {
    if (!settings.personalAccessToken) throw new Error('You must specify personalAccessToken in your creation settings');
    if (!settings.labels?.length || !settings.labels.every(x => typeof x === 'string')) throw new Error('You must specify at least one label (array of strings) in your creation settings');
    if (!settings.projects?.length || !settings.projects.every(x => typeof x === 'number')) throw new Error('You must specify at least one project id (array of numbers) in your creation settings');
    if (typeof settings.templates !== 'object' || !Object.keys(settings.templates).length) throw new Error('You must specify at least one template in your creation settings');
    if (typeof settings.closeMessage !== 'string' && typeof settings.closeMessage !== 'function') throw new Error('You must specify message to close issues with');

    this.settings = settings;
    this.debugger = new GICDebug(settings.debug);
  }

  async gitlabRequest<T extends any>(settings: GICIGitlabRequestOptions & { raw: true }): Promise<Response<T>>
  async gitlabRequest<T extends Record<string, any>>(settings: GICIGitlabRequestOptions & { raw?: false }): Promise<T>
  async gitlabRequest<T extends Record<string, any> | any>(settings: GICIGitlabRequestOptions & { raw?: boolean }): Promise<T | Response<string>> {
    if (settings.path[0] === '/') settings.path = settings.path.slice(1, settings.path.length);
    const url = `${ this.settings.entrypoint }/api/${ settings.apiVersion || 'v4' }/${ settings.path }`;

    if (!settings.headers) settings.headers = {};

    if ('form' in settings && !settings.headers?.['content-type']) {
      settings.headers['content-type'] = 'multipart/form-data';
      this.debugger.sendLog({
        prefix: 'gitlabRequest',
        message: 'Automatically applied multipart/form-data header to request form due to form setting',
        severity: 'info',
      });
    }

    if (!settings.oAuthAccessToken) {
      settings.headers['PRIVATE-TOKEN'] = settings.personalAccessToken || this.settings.personalAccessToken;

      this.debugger.sendLog({
        prefix: 'gitlabRequest',
        message: `Using private token from ${ settings.personalAccessToken
          ? 'function parameters'
          : 'template parameters' }`,
        severity: 'info',
      });
    }
    else {
      settings.headers['Authorization'] = `Bearer ${ settings.oAuthAccessToken }`;

      this.debugger.sendLog({
        prefix: 'gitlabRequest',
        message: 'Using oauth token from function parameters',
        severity: 'info',
      });
    }

    const request = got({
      url,
      method: settings.method,
      searchParams: 'query' in settings ? settings.query : undefined,
      json: 'body' in settings ? settings.body : undefined,
      body: 'form' in settings ? settings.form : undefined,
      headers: settings.headers,
    });

    if (settings.raw) return request;
    else return request.json<T>();
  }

  async getUserIdByNickname(nickname: string): Promise<number | null> {
    try {
      const response = await this.gitlabRequest<{ id: number }[]>({
        path: 'users',
        method: 'GET',
        query: {
          username: nickname,
        },
      });

      return response?.[0]?.id || null;
    }
    catch (e) {
      this.debugger.processError(e, 'getUserIdByNickname');
    }
  }

  async getIssuesToClose(options: GICIGitlabSearchParams): Promise<GICIGitlabSearchResult> {
    try {
      const searchParams = this.convertGetIssuesParamsToSearch({
        ...options,
        scope: 'all',
        sort: 'asc',
        state: 'opened',
      });

      if (options.hasOwnProperty('scope') || options.hasOwnProperty('sort') || options.hasOwnProperty('state'))
        this.debugger.sendLog({
          prefix: 'getIssuesToClose',
          message: `Options has scope, sort or state parameters. They was ignored.`,
          severity: 'warn',
        });

      for (const label of this.settings.labels) {
        searchParams.append('not[labels][]', label);
      }

      const request = await this.gitlabRequest<string>({
        path: `projects/${ options.projectId }/issues`,
        method: 'GET',
        query: searchParams,
        raw: true,
      });

      return this.parseIssuesGetResponse(request);
    }
    catch (e) {
      this.debugger.processError(e, 'getIssuesToClose');
    }
  }

  async getIssues(options: GICGitlabSearchParams): Promise<GICIGitlabSearchResult> {
    try {
      const request = await this.gitlabRequest<string>({
        path: `projects/${ options.projectId }/issues`,
        method: 'GET',
        query: this.convertGetIssuesParamsToSearch(options),
        raw: true,
      });

      return this.parseIssuesGetResponse(request);
    }
    catch (e) {
      this.debugger.processError(e, 'getIssues');
    }
  }

  async getProjectActiveMilestones(projectId: number): Promise<IGICGitlabMilestone[]> {
    return this.gitlabRequest<IGICGitlabMilestone[]>({
      path: `projects/${ projectId }/milestones`,
      method: 'GET',
      query: {
        state: 'active',
        include_parent_milestones: 'true',
      },
    });
  }

  async getLatestProjectMilestone(projectId: number): Promise<IGICGitlabMilestone | null> {
    const milestones = await this.getProjectActiveMilestones(projectId);
    /* Does only return first created active milestone */
    return milestones.sort((a, b) => new Date(a.created_at).getTime() - new Date(b.created_at).getTime())[0] || null;
  }

  async closeIssue(settings: GICIGitlabIssueAction) {
    try {
      await this.gitlabRequest({
        path: `projects/${ settings.projectId }/issues/${ settings.issueId }`,
        method: 'PUT',
        body: {
          state_event: 'close',
          discussion_locked: true,
        },
      });
      await this.createComment(settings);
    }
    catch (e) {
      this.debugger.processError(e, 'closeIssue');
    }
  }

  async createComment(settings: GICIGitlabCreateComment): Promise<void> {
    try {
      await this.gitlabRequest({
        path: `projects/${ settings.projectId }/issues/${ settings.issueId }/notes`,
        method: 'POST',
        query: {
          body: settings.comment
            || (typeof this.settings.closeMessage === 'string'
              ? this.settings.closeMessage
              : this.settings.closeMessage(settings.projectId)),
        },
      });
    }
    catch (e) {
      this.debugger.processError(e, 'createComment');
    }
  }

  protected convertGetIssuesParamsToSearch(options: GICIGitlabSearchParams | GICGitlabSearchParams): URLSearchParams {
    try {
      const settings = new URLSearchParams();

      for (const [key, value] of Object.entries(options)) {
        if (typeof value === 'string') settings.append(key, value);
        else if (typeof value === 'number') settings.append(key, value.toString());
        else if (typeof value === 'boolean') settings.append(key, value ? 'true' : 'false');
        else if (checkValidDate(value)) settings.append(key, value.toISOString());
        else if (typeof value === 'object') {
          if (key === 'iids' && Array.isArray(value) && value.every(x => typeof x === 'number')) {
            for (const id of value) settings.append('iids[]', id.toString());
            continue;
          }
          if (key === 'labels' && Array.isArray(value) && value.every(x => typeof x === 'string')) {
            settings.append('labels', value.join(','));
            continue;
          }

          throw new Error(`Invalid type of value for key ${ key } in issues params. Your object or array is invalid or not accepted for this key. Please see typescript types or Gitlab's documentation on Project's issues search.`);
        }
      }

      return settings;
    }
    catch (e) {
      this.debugger.processError(e, 'convertGetIssuesParamsToSearch');
    }
  }

  protected parseIssuesGetResponse(request: Response<string>): GICIGitlabSearchResult {
    const parsedHeaders = Object.entries(request.headers).map(x => [x[0].toLowerCase(), x[1]]);

    return {
      issues: JSON.parse(request.body),
      totalIssues: +(parsedHeaders.find(x => x[0] === 'x-total')?.[1] || '-1'),
      totalPages: +(parsedHeaders.find(x => x[0] === 'x-total-pages')?.[1] || '-1'),
      perPage: +(parsedHeaders.find(x => x[0] === 'x-per-page')?.[1] || '-1'),
    };
  }

  protected async _createIssue(options: GICTemplateCreationData<T>, ignoredIssues: Set<string>): Promise<GICGitlabIssue> {
    try {
      let {
        template: templateName,
        description,
        files,
        personalAccessToken,
        oAuthAccessToken,
        title,
        projectId,
        creationSettings = {},
      } = options;

      const template = this.settings.templates[templateName];
      if (!template)
        throw new Error(`Invalid template name: ${ templateName }. Only those template name(s) are accepted: ${ Object.keys(this.settings.templates).join(', ') }`);

      let requestSettings: Record<string, any> = {};
      const labels = new Set<string>([]);

      if (!this.settings.projects.includes(projectId))
        throw new Error(`Invalid project id: ${ projectId }. Only those project id(s) are accepted: ${ Object.values(this.settings.projects).join(', ') }`);

      if (template.prependTitle && !creationSettings.prependTitle)
        title = `${ template.prependTitle }${ title }`;
      else if (creationSettings.prependTitle)
        title = `${ creationSettings.prependTitle }${ title }`;

      const assignees: Set<number> = new Set();

      if ((!template.overwriteDefaultAssignees || creationSettings.overwriteDefaultAssignees === false) && this.settings.defaultAssigneesIds)
        for (const assignee of this.settings.defaultAssigneesIds) assignees.add(assignee);

      if (template.assigneesIds) for (const assignee of template.assigneesIds) assignees.add(assignee);
      if (creationSettings.assigneesIds) for (const assignee of creationSettings.assigneesIds) assignees.add(assignee);

      if (assignees.size > 0)
        requestSettings.assignee_ids = Array.from(assignees);

      if (template.labelsToAssign?.some(x => typeof x === 'string'))
        template.labelsToAssign.filter(x => typeof x === 'string').map(x => labels.add(x));
      if (creationSettings?.labelsToAssign?.some(x => typeof x === 'string'))
        creationSettings.labelsToAssign.filter(x => typeof x === 'string').map(x => labels.add(x));

      if (typeof template.confidential === 'boolean')
        requestSettings.confidential = template.confidential;
      if (typeof creationSettings.confidential === 'boolean')
        requestSettings.confidential = creationSettings.confidential;

      if (typeof template.dueDate === 'string')
        requestSettings.due_date = template.dueDate;
      else if (template.dueDate instanceof Date && checkValidDate(template.dueDate)) {
        requestSettings.due_date = convertDateToGitlabFormat(template.dueDate);
      }
      if (typeof creationSettings.dueDate === 'string')
        requestSettings.due_date = creationSettings.dueDate;
      else if (creationSettings.dueDate instanceof Date && checkValidDate(creationSettings.dueDate)) {
        requestSettings.due_date = convertDateToGitlabFormat(creationSettings.dueDate);
      }

      if (typeof template.epicId === 'number')
        requestSettings.epic_id = template.epicId;
      if (typeof template.epicIid === 'number')
        requestSettings.epic_iid = template.epicIid;
      if (typeof creationSettings.epicId === 'number')
        requestSettings.epic_id = creationSettings.epicId;
      if (typeof creationSettings.epicIid === 'number')
        requestSettings.epic_iid = creationSettings.epicIid;

      if (typeof template.milestoneId === 'number')
        requestSettings.milestone_id = template.milestoneId;
      else if (template.milestoneId === 'latest' && typeof creationSettings.milestoneId !== 'number' && creationSettings.milestoneId !== 'latest') {
        const milestone = await this.getLatestProjectMilestone(projectId);
        if (milestone) requestSettings.milestone_id = milestone.id;
      }
      if (typeof creationSettings.milestoneId === 'number')
        requestSettings.milestone_id = creationSettings.milestoneId;
      else if (creationSettings.milestoneId === 'latest') {
        const milestone = await this.getLatestProjectMilestone(projectId);
        if (milestone) requestSettings.milestone_id = milestone.id;
      }

      if (typeof template.weight === 'number')
        requestSettings.weight = template.weight;
      if (typeof creationSettings.weight === 'number')
        requestSettings.weight = creationSettings.weight;

      if (!Array.from(labels).some(x => this.settings.labels.includes(x))) {
        labels.add(this.settings.labels[0]);
      }

      requestSettings = {
        ...requestSettings,
        labels: Array.from(labels).join(','),
      };

      const tempTitle = `${ title }_${ new Date().getTime() }_${ Math.random() }`;
      // Avoid webhook trigger on issue create
      ignoredIssues.add(tempTitle);
      ignoredIssues.add(title);

      const issue: GICGitlabIssue = await this.gitlabRequest<GICGitlabIssue>({
        path: `projects/${ projectId }/issues`,
        method: 'POST',
        body: {
          title: tempTitle,
        },
        personalAccessToken,
        oAuthAccessToken: oAuthAccessToken,
      });

      if (!issue.iid) throw new Error(`Something went wrong after creating an issue. Expected iid parameter in returned issue, received ${ JSON.stringify(issue) }`);

      if (files) {
        const unknownFiles: string[] = [];

        for (let i = 0; i < files.length; i++) {
          const file = files[i];
          if (!file) continue;

          const formattedFile: GICTemplateCreationDataFile = file instanceof Buffer ? { file } : file;

          const form = new FormData();
          form.append('file', formattedFile.file, {
            filename: formattedFile.fileName || 'file',
            contentType: formattedFile.contentType,
            knownLength: formattedFile.knownLength,
          });

          const result = await this.gitlabRequest<IGICGitlabFile>({
            path: `projects/${ projectId }/uploads`,
            method: 'POST',
            form,
            headers: form.getHeaders(),
            personalAccessToken,
            oAuthAccessToken: oAuthAccessToken,
          });

          if (!result.markdown) continue;

          if (description.includes(`[file${ i }]`))
            description = replaceAll(description, `[file${ i }]`, result.markdown);
          else
            unknownFiles.push(result.markdown);
        }

        if (unknownFiles.length) {
          description += '  \n';
          description += unknownFiles.join('  \n');
        }
      }

      //Removes temp title
      requestSettings.title = title;
      requestSettings.description = description;

      let editedIssue = await this.gitlabRequest<GICGitlabIssue>({
        path: `projects/${ projectId }/issues/${ issue.iid }`,
        method: 'PUT',
        body: requestSettings,
      });

      if (editedIssue.state === 'closed') {
        editedIssue = await this.gitlabRequest<GICGitlabIssue>({
          path: `projects/${ projectId }/issues/${ issue.iid }`,
          method: 'PUT',
          body: {
            state_event: 'reopen',
            discussion_locked: false,
          },
        });
      }

      return editedIssue;
    }
    catch (e) {
      this.debugger.processError(e, '_createIssue');
    }
  }
}
