import { GICMainClassSettings } from '../types/main';
import { RequestError } from 'got';
import { Logger } from 'tslog';
import { GICILogFunctionSetting, GICILogFunctionSettings, GICILogSeverityLogLevels } from '../types/log';

export class GICDebug {
  loggingSettings: GICMainClassSettings<any>['debug'];

  constructor(loggingSettings: GICMainClassSettings<any>['debug']) {
    this.loggingSettings = loggingSettings;
  }

  sendLog({ prefix, message, severity }: GICILogFunctionSettings) {
    let isEnabled = false;
    let logFunction: GICILogFunctionSetting | null = null;
    let logLevels: GICILogSeverityLogLevels = ['error'];

    if (this.loggingSettings === true) isEnabled = true;
    else if (typeof this.loggingSettings === 'object') {
      isEnabled = !!this.loggingSettings.enabled;
      if (typeof this.loggingSettings.enabled === 'function') logFunction = this.loggingSettings.enabled;
      if (this.loggingSettings.logLevels) logLevels = this.loggingSettings.logLevels;
    }

    if (!isEnabled || (logLevels !== 'all' && !logLevels.includes('all') && !logLevels.includes(severity))) return;

    if (logFunction) {
      return logFunction(
        message instanceof Error
          ? message
          : `${ prefix }: ${ message }`,
      );
    }

    const logger = new Logger({
      name: prefix,
    });

    const log: keyof typeof logger = severity;
    logger[log](message);
  }

  processError(e: unknown, prefix: string): never {
    if (e instanceof RequestError) {
      let message = `Code: ${ e.response?.statusCode || 'unknown' }, message: ${ e.message || 'unknown' }, request URL: ${ e.request?.requestUrl || 'unknown' }, request method: ${ e.options?.method || 'unknown' }`;

      const body = e.response?.body;
      if (body) {
        if (typeof body === 'object') message += `, body: ${ JSON.stringify(body) }`;
        else message += `, body: ${ body }`;
      }

      this.sendLog({
        prefix: `${ prefix ? `${ prefix }-` : '' }${ e.constructor.name }`,
        message,
        severity: 'error',
      });
    }
    else if (e instanceof Error || typeof e === 'string') this.sendLog({
      prefix: prefix || 'error',
      message: e,
      severity: 'error',
    }); else this.sendLog({
      prefix: prefix || 'error',
      message: 'Unknown error',
      severity: 'error',
    });

    throw e;
  }
}
