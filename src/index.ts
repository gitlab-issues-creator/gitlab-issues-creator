import { GICTemplate } from './classes/main.class';

module.exports = GICTemplate;
export default GICTemplate;
export * from './types/main';
export * from './types/templates';
