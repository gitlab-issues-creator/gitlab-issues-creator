module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    collectCoverage: true,
    testPathIgnorePatterns: ['/dist/', '/node_modules/'],
    collectCoverageFrom: [
        'src/**/*.{ts,js,jsx}',
        '!**/node_modules/**',
        '!**/dist/**',
        '!**/coverage/**',
        '!src/index.ts',
    ],
    extensionsToTreatAsEsm: ['.ts'],
    globals: {
        'ts-jest': {
            useESM: true,
        },
    },
    reporters: [
        'default',
        ['jest-junit', { outputDirectory: 'coverage' }],
    ],
};
